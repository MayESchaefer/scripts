<?PHP
//156 0123456789abcdef
//156 " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
//$argv[1] is base10 number
//$argv[2] is base characters
$innum = intval($argv[1]);
$newbasechars = str_split($argv[2]);
$newbasenum = sizeof($newbasechars);
$workingnum = $innum;
$workingstr = "";

while ($workingnum !== 0) {
	$div = (int)($workingnum / $newbasenum);
	$rem = $workingnum % $newbasenum;
	if ($div === 0) {
		$workingstr .= $newbasechars[$rem];
		$workingnum = 0;
	}
	else {
		$workingstr .= $newbasechars[$div];
		$workingnum = $rem;
	}
}
echo $workingstr;
echo "\n";
?>